module gitlab.com/kendellfab/utmman

go 1.15

require (
	github.com/atotto/clipboard v0.1.2
	github.com/gotk3/gotk3 v0.5.1-0.20201119194643-19a453a051e2
)
