package main

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/kendellfab/utmman/internal"
)

func main() {
	gtk.Init(nil)

	mainWindow := internal.NewMainWindow("Utm Man")
	mainWindow.Display(600, 450)

	gtk.Main()
}
