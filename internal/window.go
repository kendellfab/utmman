package internal

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/url"

	"github.com/atotto/clipboard"
	"github.com/gotk3/gotk3/gdk"

	"github.com/gotk3/gotk3/gtk"
)

type MainWindow struct {
	win           *gtk.Window
	grid          *gtk.Grid
	hostEntry     *gtk.Entry
	sourceEntry   *gtk.Entry
	mediumEntry   *gtk.Entry
	campaignEntry *gtk.Entry
	termEntry     *gtk.Entry
	contentEntry  *gtk.Entry
	lblUrl        *gtk.Label
}

func NewMainWindow(title string) *MainWindow {
	win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)

	if err != nil {
		log.Fatal("Error creating main window", err)
	}

	win.Connect("destroy", func() {
		gtk.MainQuit()
	})

	iconBytes, err := base64.StdEncoding.DecodeString(Icon)
	if err != nil {
		log.Fatal("error loading icon bytes", err)
	}

	iconLoader, err := gdk.PixbufLoaderNew()
	if err != nil {
		log.Fatal("error creating icon pixbuf", err)
	}
	icon, err := iconLoader.WriteAndReturnPixbuf(iconBytes)
	if err != nil {
		log.Fatal("error writing icon bytes to pixbuf", err)
	}
	win.SetIcon(icon)

	mw := &MainWindow{win: win}
	mw.SetTile(title)
	mw.addGrid()

	return mw
}

func (mw *MainWindow) SetTile(title string) {
	mw.win.SetTitle(title)
}

func (mw *MainWindow) Display(width, height int) {
	mw.win.SetDefaultSize(width, 0)
	mw.win.ShowAll()
}

func (mw *MainWindow) addGrid() {
	grid, err := gtk.GridNew()
	if err != nil {
		log.Fatal("error creating grid", err)
	}

	hostLabel, err := gtk.LabelNew("Host")
	if err != nil {
		log.Fatal("error creating host label", err)
	}
	grid.Attach(hostLabel, 0, 0, 1, 1)

	hostEntry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("error creating host entry", err)
	}
	hostEntry.SetHExpand(true)
	mw.hostEntry = hostEntry
	grid.Attach(hostEntry, 1, 0, 1, 1)

	sourceLabel, err := gtk.LabelNew("Source")
	if err != nil {
		log.Fatal("error creating source label", err)
	}
	grid.Attach(sourceLabel, 0, 1, 1, 1)

	sourceEntry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("error creating source entry", err)
	}
	sourceEntry.SetHExpand(true)
	mw.sourceEntry = sourceEntry
	grid.Attach(sourceEntry, 1, 1, 1, 1)

	mediumLabel, err := gtk.LabelNew("Medium")
	if err != nil {
		log.Fatal("error creating medium label", err)
	}
	grid.Attach(mediumLabel, 0, 2, 1, 1)

	mediumEntry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("error creating medium entry", err)
	}
	mediumEntry.SetHExpand(true)
	mw.mediumEntry = mediumEntry
	grid.Attach(mediumEntry, 1, 2, 1, 1)

	campaignLabel, err := gtk.LabelNew("Campaign")
	if err != nil {
		log.Fatal("error creating campaign label", err)
	}
	grid.Attach(campaignLabel, 0, 3, 1, 1)
	campaignEntry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("error creating campaign entry", err)
	}
	campaignEntry.SetHExpand(true)
	mw.campaignEntry = campaignEntry
	grid.Attach(campaignEntry, 1, 3, 1, 1)

	termLabel, err := gtk.LabelNew("Term")
	if err != nil {
		log.Fatal("error creating term label", err)
	}
	grid.Attach(termLabel, 0, 4, 1, 1)
	termEntry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("error creating term entry", err)
	}
	termEntry.SetHExpand(true)
	mw.termEntry = termEntry
	grid.Attach(termEntry, 1, 4, 1, 1)

	contentLabel, err := gtk.LabelNew("Content")
	if err != nil {
		log.Fatal("error creating content label", err)
	}
	grid.Attach(contentLabel, 0, 5, 1, 1)
	contentEntry, err := gtk.EntryNew()
	if err != nil {
		log.Fatal("error creating content entry", err)
	}
	contentEntry.SetHExpand(true)
	mw.contentEntry = contentEntry
	grid.Attach(contentEntry, 1, 5, 1, 1)

	generateButton, err := gtk.ButtonNew()
	if err != nil {
		log.Fatal("error creating generate button", err)
	}
	generateButton.SetLabel("Generate")
	generateButton.SetHExpand(true)
	_, err = generateButton.Connect("clicked", mw.generateClicked)
	if err != nil {
		log.Fatal("error connecting generate click", err)
	}
	grid.Attach(generateButton, 0, 6, 2, 1)

	copyButton, _ := gtk.ButtonNewFromIconName("edit-copy", gtk.ICON_SIZE_MENU)
	copyButton.Connect("clicked", func() {
		url, err := mw.lblUrl.GetText()
		if err == nil {
			err = clipboard.WriteAll(url)
			if err != nil {
				mw.showErrorDialog(err.Error())
			}
		} else {
			mw.showErrorDialog(err.Error())
		}
	})
	grid.Attach(copyButton, 0, 7, 1, 1)
	mw.lblUrl, _ = gtk.LabelNew("")
	grid.Attach(mw.lblUrl, 1, 7, 1, 1)

	// Setup a bit better look
	grid.SetHExpand(true)
	grid.SetColumnSpacing(10)
	grid.SetRowSpacing(10)

	grid.SetMarginStart(10)
	grid.SetMarginTop(10)
	grid.SetMarginEnd(10)
	grid.SetMarginBottom(10)

	mw.grid = grid
	mw.win.Add(mw.grid)
}

func (mw *MainWindow) showErrorDialog(message string) {
	msgDialog := gtk.MessageDialogNew(mw.win, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE, message)
	msgDialog.Run()
	msgDialog.Destroy()
}

func (mw *MainWindow) generateClicked() {
	hostText, err := mw.hostEntry.GetText()
	if err != nil {
		log.Println("error getting host text", err)
	}

	sourceText, err := mw.sourceEntry.GetText()
	if err != nil {
		log.Println("error getting source text", err)
	}

	mediumText, err := mw.mediumEntry.GetText()
	if err != nil {
		log.Println("error getting medium text", err)
	}

	campaignText, err := mw.campaignEntry.GetText()
	if err != nil {
		log.Println("error getting campaign text", err)
	}

	termText, err := mw.termEntry.GetText()
	if err != nil {
		log.Println("error getting term text", err)
	}

	contentText, err := mw.contentEntry.GetText()
	if err != nil {
		log.Println("error getting content text", err)
	}

	vals := url.Values{}

	if sourceText != "" {
		vals.Set("utm_source", sourceText)
	}

	if mediumText != "" {
		vals.Set("utm_medium", mediumText)
	}

	if campaignText != "" {
		vals.Set("utm_campaign", campaignText)
	}

	if termText != "" {
		vals.Set("utm_term", termText)
	}

	if contentText != "" {
		vals.Set("utm_content", contentText)
	}

	result := fmt.Sprintf("%s?%s", hostText, vals.Encode())
	log.Println(result)
	mw.lblUrl.SetText(result)
}
